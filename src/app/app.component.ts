import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ArWindowComponent } from './ar-window/ar-window.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public dialog: MatDialog) {
  }

  showAR() {
    this.dialog.open(ArWindowComponent, {
      height: '100%',
      width: '100%',
      minWidth: '100%',
      data: 'hello ar'
    });
  }
}
