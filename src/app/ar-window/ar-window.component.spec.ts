import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArWindowComponent } from './ar-window.component';

describe('ArWindowComponent', () => {
  let component: ArWindowComponent;
  let fixture: ComponentFixture<ArWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
