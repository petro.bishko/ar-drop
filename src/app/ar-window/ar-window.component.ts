import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-ar-window',
  templateUrl: './ar-window.component.html',
  styleUrls: ['./ar-window.component.css']
})
export class ArWindowComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ArWindowComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }
}
